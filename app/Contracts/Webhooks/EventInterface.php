<?php

namespace App\Contracts\Webhooks;

interface EventInterface
{
    public static function constructFrom(array $params);

    public static function getSubscriptionPayload();

    public static function getTransactionPayload();

    public static function hasTransaction();
}
