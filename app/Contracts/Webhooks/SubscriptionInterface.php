<?php

namespace App\Contracts\Webhooks;

interface SubscriptionInterface
{
    public static function create(EventInterface $event);

    public static function update(EventInterface $event);

    public static function cancel(EventInterface $event);
}
