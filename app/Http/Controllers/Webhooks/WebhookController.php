<?php

namespace App\Http\Controllers\Webhooks;

use App\Services\Apple\AppleService;
use Illuminate\Http\Request;
use App\Services\Apple\Event;

class WebhookController
{
    public function apple(Request $request)
    {
        $payload = $request->getContent();
        $event = null;

        try {
            $event = Event::constructFrom(
                json_decode($payload, true)
            );
        } catch (\UnexpectedValueException $e) {
            // Invalid payload
            http_response_code(400);
            exit();
        }

        AppleService::handle($event);

    }

}
