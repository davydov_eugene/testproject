<?php

namespace App\Services\Apple;

use App\Contracts\Webhooks\EventInterface;
use Illuminate\Support\Collection;

class Event implements EventInterface
{
    public $payload;
    public static $subscription;

    public function __construct(array $payload)
    {
        $this->payload = $payload;
        $latestReceiptInfo = $this->latestReceiptInfo($payload)->first();

        static::$subscription = [
            'name' => 'Apple payment',
            'provider' => 'Apple webhook',
            'status' => null,
            'plan' => 'standard',
            'quantity' => $latestReceiptInfo['quantity'],
            'auto_renew_status' => $payload['auto_renew_status'] === 'true',
            'next_period_at' => $latestReceiptInfo['expires_date'],
            'trial_ends_at' => now()->toDateTimeString(),
            'api_updated_at' => now()->toDateTimeString(),
            'cancel_reason_text' => $this->getCancelReasonText($payload['expiration_intent']),
            'user_id' => 'some_user_id'
        ];

    }

    public static function constructFrom(array $payload): Event
    {
        return new static($payload);
    }

    public static function getSubscriptionPayload(): array
    {
        return static::$subscription;
    }

    public static function getTransactionPayload(): array
    {
        return [];
    }

    public static function hasTransaction(): bool
    {
        return false;
    }

    private function latestReceiptInfo(array $payload): ?Collection
    {
        if( count($payload['unified_receipt']['latest_receipt_info']) )
        {
            return collect($payload['unified_receipt']['latest_receipt_info'])
                ->sortByDesc('expires_date');
        }
        return null;
    }

    private function getCancelReasonText(int $expiration_intent): string
    {
        $reasons = [
            '1' => 'The customer voluntarily canceled their subscription.',
            '2' => 'Billing error; for example, the customer\'s payment information was no longer valid.',
            '3' => 'The customer did not agree to a recent price increase.',
            '4' => 'The product was not available for purchase at the time of renewal.',
            '5' => 'Unknown error.',
        ];

        return $reasons[$expiration_intent];
    }

}
