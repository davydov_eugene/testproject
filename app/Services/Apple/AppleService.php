<?php

namespace App\Services\Apple;

use App\Contracts\Webhooks\EventInterface;
use App\Contracts\Webhooks\SubscriptionInterface;
use App\Models\Subscription;
use App\Models\Transaction;


class AppleService implements SubscriptionInterface
{

    public static function handle(EventInterface $event)
    {
        switch ($event->payload['notification_type'])
        {
            case 'INITIAL_BUY':
                static::create($event);
                break;

            case 'DID_RENEW':
                static::update($event);
                break;

            case 'DID_FAIL_TO_RENEW':
                // TODO:: proceed Fail
                static::update($event);
                break;

            case 'CANCEL':
                // TODO:: proceed CANCEL logic
                static::cancel($event);
                break;
        }
    }

    public static function create(EventInterface $event)
    {
        // TODO: Implement create() method.

        $subscription = Subscription::create($event->getSubscriptionPayload());

        if($event->hasTransaction())
        {
            $transactionPayload = $event->getTransactionPayload();
            $transactionPayload['subscription_id'] = $subscription->id;
            Transaction::create($transactionPayload);
        }

    }

    public static function update(EventInterface $event)
    {
        // TODO: Implement update() method.
    }

    public static function cancel(EventInterface $event)
    {
        // TODO: Implement cancel() method.
    }
}
