<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSubscriptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subscriptions', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('provider');
            $table->string('status');
            $table->string('plan')->default('standard');
            $table->integer('quantity')->default(1);
            $table->boolean('auto_renew_status')->default(true);
            $table->dateTime('trial_ends_at')->nullable();
            $table->dateTime('next_period_at');
            $table->dateTime('ends_at')->nullable();
            $table->dateTime('canceled_at')->nullable();
            $table->string('cancel_reason')->nullable();
            $table->text('cancel_reason_text')->nullable();
            $table->dateTime('api_updated_at')->nullable();
            $table->string('user_id');

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subscriptions');
    }
}
