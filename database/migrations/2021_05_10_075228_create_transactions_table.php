<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->id();
            $table->unsignedSmallInteger('amount');
            $table->string('currency',3)->default('usd');
            $table->string('transaction_id');
            $table->string('original_transaction_id');
            $table->string('product_id');
            $table->string('status');

            $table->foreign('subscription_id')
                ->references('id')
                ->on('subscriptions');

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
